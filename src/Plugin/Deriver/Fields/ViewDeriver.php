<?php

namespace Drupal\graphql_fva\Plugin\Deriver\Fields;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\graphql\Utility\StringHelper;
use Drupal\graphql_views\Plugin\Deriver\ViewDeriverBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derive fields from configured views.
 */
class ViewDeriver extends ViewDeriverBase implements ContainerDeriverInterface {

  /**
   * The entity field manager instance.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The entity type manager interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Interface plugin manager instance.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected $interfacePluginManager;


  /**
   * {@inheritdoc}
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, PluginManagerInterface $interfacePluginManager) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->interfacePluginManager = $interfacePluginManager;
    parent::__construct($entity_type_manager, $interfacePluginManager);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.graphql.interface')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    // Get entity type with views arguments entity field.
    $field_map = $this->entityFieldManager->getFieldMapByFieldType('views_arguments_entity_field_item');
    if (empty($field_map)) {
      return parent::getDerivativeDefinitions($basePluginDefinition);

    }

    if (!$this->entityTypeManager->hasDefinition('view')) {
      return parent::getDerivativeDefinitions($basePluginDefinition);
    }
    $viewStorage = $this->entityTypeManager->getStorage('view');
    foreach ($field_map as $entity_type => $field_map_entity_type) {
      foreach ($field_map_entity_type as $field_name => $field) {
        foreach ($field['bundles'] as $bundle) {
          if (empty($field_definition[$entity_type][$bundle])) {
            $field_definition[$entity_type][$bundle] = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
          }

          if (empty($field_definition[$entity_type][$bundle][$field_name])) {
            continue;
          }
          $settings = $field_definition[$entity_type][$bundle][$field_name]->getSettings();
          if (empty($settings['views_id'])) {
            continue;
          }
          list($view_id, $display_id) = explode('/', $settings['views_id']);
          /** @var \Drupal\views\ViewEntityInterface $view */
          $view = $viewStorage->load($view_id);
          if (empty($view) || !$this->getRowResolveType($view, $display_id)) {
            continue;
          }

          /** @var \Drupal\graphql_views\Plugin\views\display\GraphQL $display */
          $display = $this->getViewDisplay($view, $display_id);

          $id_graphql_views = implode('-', [$view_id, $display_id, 'view']);
          $id = implode('-', [
            'views_arguments_entity',
            $bundle,
            $field_name,
          ]);
          $info = $this->getArgumentsInfo($display->getOption('arguments') ?: []);
          $arguments = [];
          $arguments += $this->getContextualArguments($info, $id_graphql_views);
          $arguments += $this->getPagerArguments($display);
          $arguments += $this->getSortArguments($display, $id_graphql_views);
          $arguments += $this->getFilterArguments($display, $id_graphql_views);
          $suffix = call_user_func_array([
            StringHelper::class,
            'camelCase',
          ], [$entity_type, $bundle]);
          $name = call_user_func_array([
            StringHelper::class,
            'camelCase',
          ], ['viewsArgumentEntity', $field_name]);
          $this->derivatives[$id] = [
              'id' => $id,
              'name' => $name,
              'type' => $display->getGraphQLResultName(),
              'parents' => [$suffix],
              'arguments' => $arguments,
              'views_arguments' => $settings['views_arguments'],
              'view' => $view,
              'display' => $display_id,
              'paged' => $this->isPaged($display),
              'arguments_info' => $info,
            ] + $this->getCacheMetadataDefinition($view, $display) + $basePluginDefinition;
        }
      }
    }


    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

}
