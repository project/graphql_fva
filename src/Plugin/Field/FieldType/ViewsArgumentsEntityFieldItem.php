<?php

namespace Drupal\graphql_fva\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Drupal\views\Views;

/**
 * Plugin implementation of the 'views argument entity field' field type.
 *
 * @FieldType(
 *   id = "views_arguments_entity_field_item",
 *   label = @Translation("Views arguments field"),
 *   category = @Translation("Reference"),
 *   default_formatter = "string",
 *   description = @Translation("This field manages configuration and
 *   presentation of views arguments on an entity."), cardinality = 1,
 * )
 */
class ViewsArgumentsEntityFieldItem extends FieldItemBase {


  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'views_id' => '',
      'views_arguments' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'target_id';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $target_type_info = \Drupal::entityTypeManager()
      ->getDefinition('view');

    $target_id_definition = DataReferenceTargetDefinition::create('string')
      ->setComputed(TRUE)
      ->setLabel(new TranslatableMarkup('@label ID', ['@label' => $target_type_info->getLabel()]));

    $target_id_definition->setRequired(TRUE);
    $properties['target_id'] = $target_id_definition;
    $properties['entity'] = DataReferenceDefinition::create('entity')
      ->setRequired(TRUE)
      ->setLabel($target_type_info->getLabel())
      ->setDescription(new TranslatableMarkup('The referenced entity'))
      // The entity object is computed out of the entity ID.
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create('view'))
      // We can add a constraint for the target entity type. The list of
      // referenceable bundles is a field setting, so the corresponding
      // constraint is added dynamically in ::getConstraints().
      ->addConstraint('EntityType', 'view');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'target_id' => [
        'description' => 'The ID of the target entity.',
        'type' => 'varchar_ascii',
        // If the target entities act as bundles for another entity type,
        // their IDs should not exceed the maximum length for bundles.
        'length' => 255,
      ],
    ];


    $schema = [
      'columns' => $columns,
      'indexes' => [
        'target_id' => ['target_id'],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $settings = $this->getSettings();
    $entity_type_manager = \Drupal::entityTypeManager();
    if ($entity_type_manager->hasDefinition('view')) {
      $options = [];
      foreach (Views::getApplicableViews('graphql_display') as list($view_id, $displayId)) {
        $options[$view_id . '/' . $displayId] = $view_id;
      }

      asort($options);
      $default_view_key = $settings['views_id'] ?? '';
      $settings_form_state = $form_state->getValue('settings');
      if (!empty($settings_form_state['views_id'])) {
        $default_view_key = $settings_form_state['views_id'];
      }
      $element['views_id'] = [
        '#type' => 'select',
        '#options' => $options,
        '#title' => t('Views'),
        '#default_value' => $default_view_key,
        '#ajax' => [
          'callback' => [$this, 'getViewsArguments'],
          'disable-refocus' => FALSE,
          'event' => 'change',
          'method' => 'replaceWith',
          'wrapper' => 'views-argument',
          // This element is updated with this AJAX callback.
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Verifying entry...'),
          ],
        ],
        '#description' => t('Show comment replies in a threaded list.'),
      ];
      $element['views_arguments'] = [
        '#type' => 'details',
        '#title' => t('Views argument'),
        '#prefix' => '<div id="views-argument">',
        '#suffix' => '</div>',
      ];
      $arguments = [];
      if (!empty($options[$default_view_key])) {

        $arguments = $this->getArgumentsByViewKey($default_view_key);
        if (empty($arguments)) {
          $element['views_arguments']['no_args'] = [
            '#type' => 'markup',
            '#markuo' => t('This views has no contextual filter'),
          ];
          return $element;
        }
      }
      $entity_type = $form['#entity'];
      $views_arguments = $settings['views_arguments'] ?? [];
      /** @var  $entity_field_manager EntityFieldManager */
      $entity_field_manager = \Drupal::service("entity_field.manager");
      $field_list = $entity_field_manager->getFieldDefinitions($entity_type->getEntityTypeId(), $entity_type->bundle());
      $options_field = [];
      foreach ($field_list as $field_name => $field) {
        $options_field[$field_name] = $field->getLabel();
      }
      $delta = 0;
      $triggering_element = $form_state->getTriggeringElement();
      $entity_type = $form['#entity'];
      /** @var  $entity_field_manager EntityFieldManager */
      $entity_field_manager = \Drupal::service("entity_field.manager");
      $field_list = $entity_field_manager->getFieldDefinitions($entity_type->getEntityTypeId(), $entity_type->bundle());
      $options_field = [];
      foreach ($field_list as $field_name => $field) {
        $options_field[$field_name] = $field->getLabel();
      }

      foreach ($arguments as $arg_name => $arg) {
        $is_parent = $views_arguments[$delta]['parent'] ?? FALSE;
        $is_parent = $settings_form_state['views_arguments'][$delta]['parent'] ?? $is_parent;

        $element['views_arguments'][$delta]['parent'] = [
          '#type' => 'checkbox',
          '#delta' => $delta,
          '#default_value' => $settings_form_state['views_arguments'][$delta]['parent'] ?? $is_parent,
          '#title' => t('Search in parent', [
            '%delta' => $delta,
            '%name' => $arg_name,
          ]),
        ];

        $name = $views_arguments[$delta]['parent'] ?? '';
        $name = $settings_form_state['views_arguments'][$delta]['parent'] ?? $name;
        $element['views_arguments'][$delta]['name'] = [
          '#type' => 'select',
          '#default_value' => $name,
          '#title' => t('Arguments n°%delta (%name)', [
            '%delta' => $delta,
            '%name' => $arg_name,
          ]),
          '#options' => $options_field,
          '#states' => [
            'visible' => [
              ':input[name="settings[views_arguments][' . $delta . '][parent]"]' => ['checked' => FALSE],
            ],
            'required' => [
              ':input[name="settings[views_arguments][' . $delta . '][parent]"]' => ['checked' => FALSE],
            ],
          ],
          '#prefix' => '<div id="search-in-parent-' . $delta . '">',
          '#suffix' => '</div>',
        ];

        $name = $views_arguments[$delta]['name_free'] ?? '';
        $name = $settings_form_state['views_arguments'][$delta]['name_free'] ?? $name;
        $element['views_arguments'][$delta]['name_free'] = [
          '#type' => 'textfield',
          '#default_value' => $name,
          '#title' => t('Arguments n°%delta (%name)', [
            '%delta' => $delta,
            '%name' => $arg_name,
          ]),
          '#states' => [
            'visible' => [
              ':input[name="settings[views_arguments][' . $delta . '][parent]"]' => ['checked' => TRUE],
            ],
            'required' => [
              ':input[name="settings[views_arguments][' . $delta . '][parent]"]' => ['checked' => TRUE],
            ],
          ],
          '#prefix' => '<div id="search-in-parent-' . $delta . '">',
          '#suffix' => '</div>',
        ];
        $name = $views_arguments[$delta]['name_free'] ?? '';
        $name = $settings_form_state['views_arguments'][$delta]['name_free'] ?? $name;
        $element['views_arguments'][$delta]['operator'] = [
          '#type' => 'select',
          '#title' => t('Operator'),
          '#default_value' => $name,
          '#options' => ['+' => t('Or'), ',' => t('And')],
          '#description' => t('Select operator if multiple', [
            '%delta' => $delta,
            '%name' => $arg_name,
          ]),
        ];
        $delta++;
      }
      $element['views_arguments']['#open'] = TRUE;
      return $element;
    }
    $element['views_id'] = [
      '#type' => 'markup',
      '#options' => t('No views with graphQL display found.'),
    ];
    return $element;
  }

  /**
   * Ajax callback views arguments.
   *
   * {@inheritDoc}
   */
  public function getViewsArguments(array &$form, FormStateInterface $form_state) {
    $settings = $form_state->getValue('settings');
    if (!empty($settings['views_id'])) {
      $arguments = $this->getArgumentsByViewKey($settings['views_id']);
      if (empty($arguments)) {
        $form['settings']['views_arguments'] = [
          '#type' => 'details',
          '#title' => t('Views argument'),
          '#prefix' => '<div id="views-argument">',
          '#suffix' => '</div>',
        ];
        $form['settings']['views_arguments']['no_args'] = [
          '#type' => 'markup',
          '#markup' => t('This views has no contextual filter'),
        ];
        return $form['settings']['views_arguments'];
      }

      $form['settings']['views_arguments']['#open'] = TRUE;
    }
    return $form['settings']['views_arguments'];
  }

  /**
   * Return arguments of a views.
   *
   * @param string $view_key
   *   The view machine name.
   *
   * @return array
   *   Array of views argument.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getArgumentsByViewKey($view_key) {
    list($view_id, $display_id) = explode('/', $view_key);
    $viewStorage = \Drupal::entityTypeManager()->getStorage('view');
    /** @var \Drupal\views\ViewEntityInterface $view */
    $view = $viewStorage->load($view_id);
    $viewExecutable = $view->getExecutable();
    $viewExecutable->setDisplay($display_id);
    $display = $viewExecutable->getDisplay();
    return $display->getOption('arguments') ?: [];
  }

}
