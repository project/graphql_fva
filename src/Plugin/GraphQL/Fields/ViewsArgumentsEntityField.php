<?php

namespace Drupal\graphql_fva\Plugin\GraphQL\Fields;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\FieldPluginManager;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Retrieve the views and inject field inside arguments.
 *
 * @GraphQLField(
 *   id = "views_arguments_entity_field",
 *   secure = true,
 *   name = "viewsArgumentEntity",
 *   deriver =
 *   "Drupal\graphql_fva\Plugin\Deriver\Fields\ViewDeriver"
 * )
 */
class ViewsArgumentsEntityField extends FieldPluginBase implements ContainerFactoryPluginInterface {

  use LoggerChannelTrait;
  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A field plugin manager instance.
   *
   * @var \Drupal\graphql\Plugin\FieldPluginManager
   */
  protected $fieldPluginManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $pluginId,
    $pluginDefinition,
    EntityTypeManagerInterface $entityTypeManager,
    FieldPluginManager $fieldPluginManager

  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fieldPluginManager = $fieldPluginManager;
    parent::__construct($configuration, $pluginId, $pluginDefinition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.graphql.field')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {

    /** @var \Drupal\views\Entity\View $view */
    $plugin_definition = $this->getPluginDefinition();
    $view = $plugin_definition['view'];

    if (!empty($view) && !empty($plugin_definition) && $value instanceof EntityInterface) {
      $executable = $view->getExecutable();
      $executable->setDisplay($plugin_definition['display']);
      // Set view contextual filters.
      /* @see \Drupal\graphql_core\Plugin\Deriver\ViewDeriverBase::getArgumentsInfo() */
      if (!empty($plugin_definition['arguments_info'])) {
        $arguments = $this->extractContextualFilters($value, $args);
        $executable->setArguments($arguments);
      }
      $filters = $executable->getDisplay()->getOption('filters');
      $input = $this->extractExposedInput($value, $args, $filters);
      $executable->setExposedInput($input);

      // This is a workaround for the Taxonomy Term filter which requires a full
      // exposed form to be sent OR the display being an attachment to just
      // accept input values.
      $executable->is_attachment = TRUE;
      $executable->exposed_raw_input = $input;

      if (!empty($plugin_definition['paged'])) {
        // Set paging parameters.
        $executable->setItemsPerPage($args['pageSize']);
        $executable->setCurrentPage($args['page']);
      }
      // @todo conf or dynamic with targrte cardinality
      $views_set_args = [];
      foreach ($plugin_definition['views_arguments'] as $delta => $views_argument) {
        $args = [];
        if ($views_argument['parent'] == TRUE && method_exists($value, 'getParentEntity')) {
          if (empty($views_argument['name_free'])) {
            $this->getLogger('graphql_fva')
              ->error(t('The is no field name for delta %delta', ['%delta' => $delta]));
            continue;
          }
          $parent = $value->getParentEntity();
          if ($parent instanceof EntityInterface && $parent->hasField($views_argument['name_free'])) {
            $args = $this->buildArgsByEntity($views_argument['name_free'], $parent);
          }
        }

        // Search in entity the field.
        if ($views_argument['parent'] == FALSE && $value->hasField($views_argument['name'])) {
          if (empty($views_argument['name'])) {
            $this->getLogger('graphql_fva')
              ->error(t('The is no field name for delta %delta', ['%delta' => $delta]));
            continue;
          }
          $args = $this->buildArgsByEntity($views_argument['name'], $value);
        }
        if (!empty($args)) {
          $views_set_args[$delta] = implode($views_argument['operator'], $args);
        }
      }
      // Insert args inside views.
      $executable->setArguments($views_set_args);
      $result = $executable->render($plugin_definition['display']);
      /** @var \Drupal\Core\Cache\CacheableMetadata $cache */
      if ($cache = $result['cache']) {
        $cache->setCacheContexts(
          array_filter($cache->getCacheContexts(), function ($context) {
            // Don't emit the url cache contexts.
            return $context !== 'url' && strpos($context, 'url.') !== 0;
          })
        );
      }
      yield $result;
    }
  }

  /**
   * @param $field_name
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return array
   */
  protected function buildArgsByEntity($field_name, EntityInterface $entity) {
    $args = [];
    $field = $entity->get($field_name);
    foreach ($field->getIterator() as $field_item) {
      $main_property = $field_item->mainPropertyName();
      $field_value = $field_item->getValue();
      if (!empty($field_value[$main_property])) {
        $args[] = $field_value[$main_property];
      }
    }
    return $args;
  }

  /**
   * {@inheritdoc}
   */
  protected function getCacheDependencies(array $result, $value, array $args, ResolveContext $context, ResolveInfo $info) {
    return array_map(function ($item) {
      return $item['cache'];
    }, $result);
  }

  /**
   * Retrieves the contextual filter argument from the parent value or args.
   *
   * @param $value
   *   The resolved parent value.
   * @param $args
   *   The arguments provided to the field.
   *
   * @return array
   *   An array of arguments containing the contextual filter value from the
   *   parent or provided args if any.
   */
  protected function extractContextualFilters($value, $args) {
    $definition = $this->getPluginDefinition();
    $arguments = [];
    foreach ($definition['arguments_info'] as $argumentId => $argumentInfo) {
      if (isset($args['contextualFilter'][$argumentId])) {
        $arguments[$argumentInfo['index']] = $args['contextualFilter'][$argumentId];
        continue;
      }
      if (
        $value instanceof EntityInterface &&
        $value->getEntityTypeId() === $argumentInfo['entity_type'] &&
        (empty($argumentInfo['bundles']) ||
          in_array($value->bundle(), $argumentInfo['bundles'], TRUE))
      ) {
        $arguments[$argumentInfo['index']] = $value->id();
        continue;
      }
      $arguments[$argumentInfo['index']] = NULL;

    }

    return $arguments;
  }

  /**
   * Retrieves sort and filter arguments from the provided field args.
   *
   * @param $value
   *   The resolved parent value.
   * @param $args
   *   The array of arguments provided to the field.
   * @param $filters
   *   The available filters for the configured view.
   *
   * @return array
   *   The array of sort and filter arguments to execute the view with.
   */
  protected function extractExposedInput($value, $args, $filters) {
    // Prepare arguments for use as exposed form input.
    $input = array_filter([
      // Sorting arguments.
      'sort_by' => isset($args['sortBy']) ? $args['sortBy'] : NULL,
      'sort_order' => isset($args['sortDirection']) ? $args['sortDirection'] : NULL,
    ]);

    // If some filters are missing from the input, set them to an empty string
    // explicitly. Otherwise views module generates "Undefined index" notice.
    foreach ($filters as $filterRow) {
      if (!isset($filterRow['expose']['identifier'])) {
        continue;
      }

      $inputKey = $filterRow['expose']['identifier'];
      if (!isset($args['filter'][$inputKey])) {
        $input[$inputKey] = $filterRow['value'];
        continue;
      }
      $input[$inputKey] = $args['filter'][$inputKey];
    }

    return $input;
  }

}
